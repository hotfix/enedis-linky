<?php

namespace Hotfix\EnedisLinky;

use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;

class GuzzleClientFactory
{
    public static $historyGuzzle = [];

    public static function historyGuzzleClient(): Client
    {
        $history = Middleware::history(self::$historyGuzzle);

        $stack = HandlerStack::create();
        $stack->push($history);

        return new Client(
            [
                'handler' => $stack,
                'allow_redirects' => false,
                'headers' => self::getDefaultHeaders(),
                'cookies' => true,
            ]
        );
    }

    public static function getDefaultHeaders(): array
    {
        return [
            'User-Agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36',
            'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            'Accept-Encoding' => 'gzip, deflate',
            'Accept-Language' => 'fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7,es;q=0.6,pt;q=0.5',
            'Referer' => 'https://www.enedis.fr/',
            'DNT' => '1',
            'Cache-Control' => 'no-cache',
        ];
    }

    public static function defaultGuzzleClient(): Client
    {
        return new Client(
            [
                'allow_redirects' => false,
                'headers' => self::getDefaultHeaders(),
                'cookies' => true,
            ]
        );
    }

    public static function displayHistoryGuzzleClient(): void
    {
        foreach (self::$historyGuzzle as $transaction) {
            echo $transaction['request']->getMethod().' ';
            echo $transaction['request']->getUri().PHP_EOL;
            foreach ($transaction['request']->getHeaders() as $key => $values) {
                foreach ($values as $value) {
                    printf('%s: %s'.PHP_EOL, $key, $value);
                }
            }

            if ($content = $transaction['request']->getBody()) {
                echo $content.PHP_EOL;
            }
            echo PHP_EOL;

            //> GET, HEAD
            if ($transaction['response']) {
                echo $transaction['response']->getStatusCode().' ';
                echo $transaction['response']->getReasonPhrase().PHP_EOL;
                foreach ($transaction['response']->getHeaders() as $key => $values) {
                    foreach ($values as $value) {
                        printf('%s: %s'.PHP_EOL, $key, $value);
                    }
                }
                echo PHP_EOL;
                echo $transaction['response']->getBody()->getContents();
                echo PHP_EOL;
                //> 200, 200
            } elseif ($transaction['error']) {
                echo $transaction['error'];
                //> exception
            }
        }
    }
}

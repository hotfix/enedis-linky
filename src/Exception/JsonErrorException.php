<?php

namespace Hotfix\EnedisLinky\Exception;

use Exception;
use Throwable;

class JsonErrorException extends Exception
{
    public function __construct(Throwable $previous = null)
    {
        parent::__construct(json_last_error_msg(), json_last_error(), $previous);
    }
}

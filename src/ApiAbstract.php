<?php

namespace Hotfix\EnedisLinky;

use DateTime;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJarInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\Uri;
use GuzzleHttp\TransferStats;
use Hotfix\EnedisLinky\Exception\EnedisException;
use Hotfix\EnedisLinky\Exception\HttpException;
use Hotfix\EnedisLinky\Exception\JsonErrorException;
use Hotfix\EnedisLinky\Exception\LogicException;
use Hotfix\EnedisLinky\Exception\LoginException;
use Psr\Http\Message\ResponseInterface;

abstract class ApiAbstract
{
    protected $client;
    protected $referrer;

    public function login(string $username, string $password): self
    {
        $client = $this->getClient();

        try {
            $response = $client->post(
                UriConfig::login(),
                [
                    'allow_redirects' => false,
                    'headers' => array_merge(
                        $client->getConfig('headers'),
                        [
                            'Referer' => UriConfig::login(),
                        ]
                    ),
                    'form_params' => [
                        'IDToken1' => $username,
                        'IDToken2' => $password,
                        'Login.Submit' => 'accéder+à+mon+compte',
                        'goto' => base64_encode(UriConfig::homepage()),
                        'gotoOnFail' => '',
                        'SunQueryParamsString' => base64_encode('realm=particuliers'),
                        'encoded' => 'true',
                        'gx_charset' => 'UTF-8',
                    ],
                ]
            );
        } catch (ClientException $e) {
            throw new HttpException('An HTTP error has occurred.', $e->getCode(), $e);
        }

        if (302 === $response->getStatusCode()) {
            $this->throwExceptionMaintenanceMode($response);

            $cookieJar = $client->getConfig('cookies');
            if (!$cookieJar instanceof CookieJarInterface) {
                throw new LogicException(
                    'For work correctly Guzzle Client must be active cookies options, see http://docs.guzzlephp.org/en/stable/quickstart.html#cookies'
                );
            }

            foreach ($cookieJar->getIterator() as $cookie) {
                if ('iPlanetDirectoryPro' === $cookie->getName()) {
                    return $this->loadHomepage($response);
                }
            }
        }

        throw new LoginException('Sorry, could not connect. Check your credentials.');
    }

    public function getClient(): Client
    {
        if (!$this->client instanceof Client) {
            $this->client = GuzzleClientFactory::defaultGuzzleClient();
        }

        return $this->client;
    }

    public function setClient(Client $client): self
    {
        $this->client = $client;

        return $this;
    }

    protected function loadHomepage(ResponseInterface $response): self
    {
        $location = $response->getHeader('location');
        if (is_array($location) && array_key_exists(0, $location)) {
            $location = $location[0];
        } else {
            $location = UriConfig::homepage();
        }

        try {
            $referer = null;
            $client = $this->getClient();
            $response = $client->get(
                $location,
                [
                    'allow_redirects' => true,
                    'on_stats' => function (TransferStats $stats) use (&$referer) {
                        $referer = $stats->getEffectiveUri();
                    },
                ]
            );
        } catch (ClientException $e) {
            throw new HttpException('An HTTP error has occurred.', $e->getCode(), $e);
        }

        $this->throwExceptionMaintenanceMode($response);
        if (200 !== $response->getStatusCode()) {
            throw new LoginException(
                sprintf('Sorry, error when redirect to homepage after connect. Maybe check your credentials. (%s)', $response->getReasonPhrase()),
                $response->getStatusCode()
            );
        }

        if ($referer instanceof Uri) {
            $this->setReferrer((string)$referer);
        } else {
            $this->setReferrer(UriConfig::homepage());
        }

        return $this;
    }

    protected function queryData(
        string $resourceId,
        ?DateTime $startDate = null,
        ?DateTime $endDate = null
    ): array {
        $client = $this->getClient();
        $headers = $client->getConfig('headers');

        try {
            $response = $client->post(
                $this->urlResource($resourceId),
                [
                    'allow_redirects' => false,
                    'form_params' => $this->formParams($resourceId, $startDate, $endDate),
                    'headers' => array_merge(
                        $headers,
                        [
                            'Referer' => $this->getReferrer(),
                            'Accept' => 'application/json, text/javascript, */*; q=0.01',
                            'X-Requested-With' => 'XMLHttpRequest',
                        ]
                    ),
                ]
            );
        } catch (ClientException $e) {
            throw new HttpException('An HTTP error has occurred.', $e->getCode(), $e);
        }

        $this->throwExceptionMaintenanceMode($response);
        if (200 !== $response->getStatusCode()) {
            throw new HttpException($response->getReasonPhrase(), $response->getStatusCode());
        }

        $data = json_decode($response->getBody()->getContents(), true);
        if (JSON_ERROR_NONE !== json_last_error()) {
            throw new JsonErrorException();
        }

        if (
            array_key_exists('etat', $data)
            && is_array($data['etat'])
            && array_key_exists('valeur', $data['etat'])
            && 'erreur' === $data['etat']['valeur']
        ) {
            $message =
                (array_key_exists('erreurText', $data['etat']))
                    ? $data['etat']['erreurText'] : 'Erreur inconnus.';

            throw new EnedisException($message);
        }

        return $data;
    }

    abstract public function urlResource(string $resourceId): string;

    abstract public function formParams(
        string $resourceId,
        ?DateTime $startDate = null,
        ?DateTime $endDate = null
    ): array;

    public function getReferrer(): ?string
    {
        return $this->referrer;
    }

    public function setReferrer(string $referrer): self
    {
        $this->referrer = $referrer;

        return $this;
    }

    public function throwExceptionMaintenanceMode(ResponseInterface $response)
    {
        $location = $response->getHeader('location');
        if (is_array($location) && array_key_exists(0, $location)) {
            $location = $location[0];
        }

        if ($location === UriConfig::maintenance()) {
            throw new EnedisException('Website site enedis is on maintenance mode.');
        }
    }
}

This API being essentially to collect our french electricity consumption, this page is in ... French!

Intro
-----
Voici une API simple d'utilisation pour récupérer vos données de consommations et de production du compteur Linky.

J'utilise personnellement cette API avec une tâche planifiée (cron) toutes les 8h pour enregistrer l'ensemble des données dans un fichier json. 
Ce qui me permet de conserver mes données, et de les afficher sur un joli graphique, pour faire des corrélations...

Pré-requis
----------
- Un compteur Linky !
- Un compte Enedis. [https://espace-client-particuliers.enedis.fr/web/espace-particuliers/accueil](Vous pouvez le créer dès maintenant). Vous devez attendre quelques semaines après l'installation du Linky pour voir vos données sur le site Enedis. Une fois ces données disponible, vous pouvez utiliser cette API.
- Un serveur PHP 7.2 minimun avec accès à internet (mutualisé sur hébergement, NAS Synology, etc.)

Installation via composer
-------------------------
```bash
composer require hotfix/enedis-linky
```

Exemple Consomation
---------------------
```php
$consumption = (new Hotfix\EnedisLinky\Consumption())
    ->login('account@email.com', 'password');

$perDay = $consumption->perDay(new \DateTime('01-01-2019'), new \DateTime('09-01-2019'));
$perMonth = $consumption->perMonth(new \DateTime('01-01-2018'), new \DateTime('31-12-2018'));
$perYear = $consumption->perYear();
```
Exemple Production
---------------------
```php
$production = (new Hotfix\EnedisLinky\Production())
    ->login('account@email.com', 'password');

$perDay = $production->perDay(new \DateTime('01-01-2019'), new \DateTime('09-01-2019'));
//   array:9 [
//     "2019-01-01" => array:2 [
//       "consumption" => 0.02
//       "production" => 1.91
//     ]
//     "2019-01-02" => array:2 [
//       "consumption" => 0.021
//       "production" => 12.471
//     ]
//     ...
//   ];


$perMonth = $production->perMonth(new \DateTime('01-01-2019'), new \DateTime('31-12-2019'));
//   array:9 [
//     "2019-01" => array:2 [
//       "consumption" => 0.573
//       "production" => 394.517
//     ]
//     "2019-02" => array:2 [
//       "consumption" => 0.402
//       "production" => 796.386
//     ]
//     ...
//   ];

$perYear = $production->perYear();
//   array:2 [
//     "2018" => array:2 [
//       "consumption" => 0.824
//       "production" => 789.989
//     ]
//     "2019" => array:2 [
//       "consumption" => 2.065
//       "production" => 5737.738
//     ]
//   ];
```

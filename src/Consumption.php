<?php

namespace Hotfix\EnedisLinky;

use DateTime;
use Hotfix\EnedisLinky\Exception\LogicException;

class Consumption extends ApiAbstract
{
    public function urlResource(string $resourceId): string
    {
        return UriConfig::resource($resourceId, UriConfig::RESSOURCE_CONSUMPTION_ID);
    }

    public function formParams(
        string $resourceId,
        ?DateTime $startDate = null,
        ?DateTime $endDate = null
    ): array {
        $formParams = [];
        switch ($resourceId) {
            case 'urlCdcJour':
            case 'urlCdcMois':
                if ($startDate instanceof DateTime && $endDate instanceof DateTime) {
                    $formParams = [
                        sprintf('_%s_dateDebut', UriConfig::RESSOURCE_CONSUMPTION_ID) => $startDate->format('d/m/Y'),
                        sprintf('_%s_dateFin', UriConfig::RESSOURCE_CONSUMPTION_ID) => $endDate->format('d/m/Y'),
                    ];
                }
                break;
        }

        return $formParams;
    }

    public function perDay(DateTime $startDate, DateTime $endDate): array
    {
        $rawData = $this->queryData('urlCdcJour', $startDate, $endDate);
        $rawData = $this->sliceData($rawData);

        return $this->formatData($rawData['graphe']['data'], $startDate, 'Y-m-d', '+%d day');
    }

    private function sliceData(array $rawData): array
    {
        if (array_key_exists('decalage', $rawData['graphe']) && $rawData['graphe']['decalage'] > 0) {
            $decalage = $rawData['graphe']['decalage'];

            $rawData['graphe']['data'] = array_slice($rawData['graphe']['data'], $decalage, $decalage * -1);
        }

        return $rawData;
    }

    protected function formatData(array $raw, DateTime $date, string $dateFormat, string $modifyFormat): array
    {
        $data = [];
        foreach ($raw as $key => $row) {
            $key = (clone $date)
                ->modify(sprintf($modifyFormat, $key))
                ->format($dateFormat);

            $data[$key] = (-2 == $row['valeur']) ? null : $row['valeur'];
        }

        return $data;
    }

    public function perMonth(DateTime $startDate, DateTime $endDate): array
    {
        $startDate->modify('first day of this month');
        $endDate->modify('last day of this month');

        $rawData = $this->queryData('urlCdcMois', $startDate, $endDate);
        $rawData = $this->sliceData($rawData);

        return $this->formatData($rawData['graphe']['data'], $startDate, 'Y-m', '+%d month');
    }

    public function perYear(): array
    {
        $rawData = $this->queryData('urlCdcAn');
        $rawData = $this->sliceData($rawData);

        $date = DateTime::createFromFormat('d/m/Y', $rawData['graphe']['periode']['dateDebut']);
        if ($date instanceof DateTime) {
            return $this->formatData($rawData['graphe']['data'], $date, 'Y', '+%d year');
        }

        throw new LogicException('Invalid format date find.');
    }
}

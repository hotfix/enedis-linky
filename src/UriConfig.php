<?php

namespace Hotfix\EnedisLinky;

class UriConfig
{
    const BASE_LOGIN = 'https://espace-client-connexion.enedis.fr';
    const AUTH_LOGIN = '/auth/UI/Login';

    const BASE = 'https://espace-client-particuliers.enedis.fr';
    const MAINTENANCE = '/messages/maintenance.html';

    const HOMEPAGE_CONSUMPTION = '/group/espace-particuliers/accueil';
    const RESSOURCE_CONSUMPTION = '/group/espace-particuliers/suivi-de-consommation';
    const RESSOURCE_PRODUCTION = '/group/espace-particuliers/mes-donnees-de-production-prod';

    const RESSOURCE_CONSUMPTION_ID = 'lincspartdisplaycdc_WAR_lincspartcdcportlet';
    const RESSOURCE_PRODUCTION_ID = 'partproddonneesprod_WAR_lincspartportlet_INSTANCE_prodDonneesProd';

    public static function login(): string
    {
        return sprintf('%s%s', self::BASE_LOGIN, self::AUTH_LOGIN);
    }

    public static function homepage(): string
    {
        return sprintf('%s%s', self::BASE, self::HOMEPAGE_CONSUMPTION);
    }

    public static function maintenance(): string
    {
        return sprintf('%s%s', self::BASE, self::MAINTENANCE);
    }

    public static function resource(string $resourceId, string $ppid): string
    {
        $resourceUrl = (self::RESSOURCE_CONSUMPTION_ID === $ppid) ? self::RESSOURCE_CONSUMPTION : self::RESSOURCE_PRODUCTION;

        return sprintf(
            '%s%s?%s',
            self::BASE,
            $resourceUrl,
            http_build_query(
                [
                    'p_p_id' => $ppid,
                    'p_p_lifecycle' => 2,
                    'p_p_state' => 'normal',
                    'p_p_mode' => 'view',
                    'p_p_resource_id' => $resourceId,
                    'p_p_cacheability' => 'cacheLevelPage',
                    'p_p_col_id' => 'column-1',
                    'p_p_col_count' => '2',
                ]
            )
        );
    }
}

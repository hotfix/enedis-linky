<?php

namespace Hotfix\EnedisLinky;

use DateTime;

class Production extends ApiAbstract
{
    public function urlResource(string $resourceId): string
    {
        return UriConfig::resource($resourceId, UriConfig::RESSOURCE_PRODUCTION_ID);
    }

    public function formParams(
        string $resourceId,
        ?DateTime $startDate = null,
        ?DateTime $endDate = null
    ): array {
        $formParams = [];
        switch ($resourceId) {
            case 'urlCdcJour':
            case 'urlCdcMois':
                if ($startDate instanceof DateTime && $endDate instanceof DateTime) {
                    $formParams = [
                        sprintf('_%s_beginDate', UriConfig::RESSOURCE_PRODUCTION_ID) => $startDate->format('d/m/Y'),
                        sprintf('_%s_endDate', UriConfig::RESSOURCE_PRODUCTION_ID) => $endDate->format('d/m/Y'),
                    ];
                }
                break;
        }

        return $formParams;
    }

    public function perMonth(DateTime $startDate, DateTime $endDate): array
    {
        $startDate->modify('first day of this month');
        $endDate->modify('last day of this month');

        $rawData = $this->queryData('urlCdcMois', $startDate, $endDate);
        if (empty($rawData)) {
            return [];
        }

        return $this->formatData($rawData, $startDate, 'Y-m', '+%d month');
    }

    protected function formatData(array $raw, DateTime $date, string $dateFormat, string $modifyFormat): array
    {
        $data = [];
        foreach ($raw['prodValueList'] as $key => $value) {
            $key = (clone $date)
                ->modify(sprintf($modifyFormat, $key))
                ->format($dateFormat);

            $data[$key] = [
                'consumption' => 0,
                'production' => (-1 == $value) ? 0 : $value,
            ];
        }

        foreach ($raw['consValueList'] as $key => $value) {
            $key = (clone $date)
                ->modify(sprintf($modifyFormat, $key))
                ->format($dateFormat);

            $data[$key]['consumption'] = (-1 == $value) ? 0 : $value;
        }

        return $data;
    }

    public function perDay(DateTime $startDate, DateTime $endDate): array
    {
        $rawData = $this->queryData('urlCdcJour', $startDate, $endDate);

        return $this->formatData($rawData, $startDate, 'Y-m-d', '+%d day');
    }

    public function perYear(): array
    {
        $rawData = $this->queryData('urlCdcAnnees');

        $start = new DateTime();
        $start->modify(sprintf('-%d years', count($rawData['prodValueList']) - 1));

        return $this->formatData($rawData, $start, 'Y', '+%d year');
    }
}

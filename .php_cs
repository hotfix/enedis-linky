<?php

/**
 * For more information about PHP-CS-Fixer configuration files and options, check
 * https://github.com/FriendsOfPHP/PHP-CS-Fixer.
 */

return PhpCsFixer\Config::create()
    ->setRiskyAllowed(true)
    ->setRules([
        '@PSR2' => true,
        '@Symfony' => true,
        '@Symfony:risky' => true,
        'native_function_invocation' => false,
        'random_api_migration' => true,
        'array_syntax' => [
            'syntax' => 'short',
        ],
    ])
    ->setFinder(
        PhpCsFixer\Finder::create()
            ->in(__DIR__.'/src')
            ->append([__FILE__])
    );
